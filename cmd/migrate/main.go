package main

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/driver/mongodb"
	"gitlab.com/rathil/migrate/driver/postgres"
	"log"
	"os"
)

func init() {
	addDriver(mongodb.Get())
	addDriver(postgres.Get())
}

func main() {
	if err := (&cli.App{
		Name: "Migrate DB",
		Commands: []*cli.Command{
			{
				Name:        "create",
				Aliases:     []string{"c"},
				Usage:       "Create new migration",
				Subcommands: createCmd(),
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "name",
						Aliases:  []string{"n"},
						Usage:    "Name of new migration",
						Required: true,
					},
					&cli.PathFlag{
						Name:     "path",
						Aliases:  []string{"p"},
						Usage:    "Path to create migration templates",
						Required: true,
					},
				},
			},
			{
				Name:        "up",
				Usage:       "Applying migrations",
				Subcommands: upCmd(),
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:     "path",
						Aliases:  []string{"p"},
						Usage:    "Path to migrations",
						Required: true,
					},
					&cli.PathFlag{
						Name:    "registry-name",
						Aliases: []string{"rn"},
						Usage:   "Name of the collection/table to register migrations",
						Value:   "migrations",
					},
				},
			},
			{
				Name:        "down",
				Usage:       "Cancel migrations",
				Subcommands: downCmd(),
				Flags: []cli.Flag{
					&cli.PathFlag{
						Name:     "path",
						Aliases:  []string{"p"},
						Usage:    "Path to migrations",
						Required: true,
					},
					&cli.PathFlag{
						Name:    "registry-name",
						Aliases: []string{"rn"},
						Usage:   "Name of the collection/table to register migrations",
						Value:   "migrations",
					},
				},
			},
		},
		Authors: []*cli.Author{
			{
				Name:  "Ievgenii Kyrychenko",
				Email: "rathil@rathil.com",
			},
		},
		EnableBashCompletion: true,
	}).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
