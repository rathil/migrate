package main

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/driver"
)

var (
	drivers []driver.IDriver
)

func addDriver(driver driver.IDriver) {
	drivers = append(drivers, driver)
}

func createCmd() []*cli.Command {
	result := make([]*cli.Command, 0, len(drivers))
	for _, d := range drivers {
		result = append(result, d.CmdCreate())
	}
	return result
}

func upCmd() []*cli.Command {
	result := make([]*cli.Command, 0, len(drivers))
	for _, d := range drivers {
		result = append(result, d.CmdUp())
	}
	return result
}

func downCmd() []*cli.Command {
	result := make([]*cli.Command, 0, len(drivers))
	for _, d := range drivers {
		result = append(result, d.CmdDown())
	}
	return result
}
