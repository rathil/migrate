## General Information

Migration component that allows you to apply migrations to the database and record the status of migrations applied. If
one of the migrations is not applied, all applied migrations are cancelled.

## Examples

- for [MongoDB](examples/mongodb/)
- for [PostgreSQL](examples/postgres/)

## Use

### Add migrate as a dependency to the project

```shell
go get gitlab.com/rathil/migrate@v1
```

### Install migrate as a cli program

```shell
go install gitlab.com/rathil/migrate/cmd/migrate@v1
```