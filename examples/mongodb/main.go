package main

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/driver/mongodb"
	"gitlab.com/rathil/migrate/filesystem"

	"log"
	"os"
)

func main() {
	if err := (&cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "uri",
				Usage:    "Database connection string",
				Required: true,
			},
			&cli.PathFlag{
				Name:    "registry-name",
				Aliases: []string{"rn"},
				Usage:   "Name of the collection/table to register migrations",
				Value:   "migrations",
			},
		},
		Commands: []*cli.Command{
			{
				Name: "up",
				Action: func(ctx *cli.Context) error {
					return mongodb.Get().UpFS(
						ctx.Context,
						ctx.String("uri"),
						ctx.String("registry-name"),
						filesystem.FSPath("./migrations"),
					)
				},
			},
			{
				Name: "down",
				Flags: []cli.Flag{
					&cli.UintFlag{
						Name:     "count",
						Aliases:  []string{"c"},
						Usage:    "Number of migrations to cancel",
						Required: true,
					},
				},
				Action: func(ctx *cli.Context) error {
					return mongodb.Get().DownFS(
						ctx.Context,
						ctx.String("uri"),
						ctx.String("registry-name"),
						filesystem.FSPath("./migrations"),
						ctx.Int("count"),
					)
				},
			},
		},
		Authors: []*cli.Author{
			{
				Name:  "Ievgenii Kyrychenko",
				Email: "rathil@rathil.com",
			},
		},
	}).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
