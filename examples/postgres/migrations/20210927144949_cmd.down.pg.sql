-- if you want to change the query delimiter, leave this comment and change the delimiter to the needed one
-- +delimiter ;
-- 
-- Example:
-- alter table device add name varchar;
-- update device d set name = db.name from device_backup db where d.id = db.id;
-- drop table device_backup;
