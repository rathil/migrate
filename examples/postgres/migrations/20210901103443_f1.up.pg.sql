-- +delimiter &
create table device
(
    id   bigint not null
        constraint device_pk primary key,
    name varchar
)&
insert into device
values (1, '111'),
       (2, '222'),
       (3, '333'),
       (4, '444'),
       (5, '555')