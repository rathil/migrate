-- if you want to change the query delimiter, leave this comment and change the delimiter to the needed one
-- +delimiter ;
--
-- Example:
create table device_backup
(
	id bigint not null constraint device_backup_pk primary key,
	name varchar
);
comment on table device_backup is 'Backup for table "device" before delete field "name"';
comment on column device_backup.id is 'Original device id';
comment on column device_backup.name is 'The name of the device to be saved until the next release';

insert into device_backup select id, name from device;
alter table device drop column name;
