package helper

import (
	"path"

	"gitlab.com/rathil/migrate/filesystem"
)

func Find(
	fs filesystem.FS,
	dir string,
) ([]string, error) {
	items, err := fs.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	var result []string
	for _, item := range items {
		if item.IsDir() {
			files, errFind := Find(fs, path.Join(dir, item.Name()))
			if errFind != nil {
				return nil, errFind
			}
			result = append(result, files...)
		} else {
			result = append(result, path.Join(dir, item.Name()))
		}
	}
	return result, nil
}
