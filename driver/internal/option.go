package internal

type (
	IOption interface {
		option()
	}
	Option struct{}
)

func (a Option) option() {}
