package driver

import (
	"context"

	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/driver/internal"
	"gitlab.com/rathil/migrate/filesystem"
)

type IDriver interface {
	// Name of driver
	Name() string
	// CmdCreate - get command for create new transaction
	CmdCreate() *cli.Command
	// CmdUp - get command for apply transactions
	CmdUp() *cli.Command
	// CmdDown - get command for cancel transactions
	CmdDown() *cli.Command
	// Up - apply transactions
	// Deprecated. Use UpFS
	Up(
		c context.Context,
		uri string,
		registryName string,
		path string,
	) error
	// UpFS - apply transactions
	UpFS(
		c context.Context,
		uri string,
		registryName string,
		fs filesystem.FS,
		options ...internal.IOption,
	) (err_ error)
	// Down - cancel transactions
	// Deprecated. Use DownFS
	Down(
		c context.Context,
		uri string,
		registryName string,
		path string,
		count int,
	) error
	// DownFS - cancel transactions
	DownFS(
		c context.Context,
		uri string,
		registryName string,
		fs filesystem.FS,
		count int,
		options ...internal.IOption,
	) error
}
