package postgres

import (
	"bytes"
	"context"
	"errors"
	"io"
	"os"
	"os/exec"
	"path"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/rathil/migrate/filesystem"
)

type migrations []migration
type migration struct {
	fs       filesystem.FS
	name     string
	upPath   string
	downPath string
}

var (
	errNoDown           = errors.New("no migrations to cancel")
	regexCommentCleanup = regexp.MustCompile(`(?m)^--[^\r\n]*`)
	regexCmd            = regexp.MustCompile(`(?m)^--\s*\+cmd\s*([^\r\n]+)`)
	regexDelimiter      = regexp.MustCompile(`(?m)^--\s*\+delimiter\s*([^\r\n])`)
)

func createMigration(
	fs filesystem.FS,
	files []string,
	orderAsc bool,
) migrations {
	sort.SliceStable(files, func(i, j int) bool {
		if orderAsc {
			return path.Base(files[i]) < path.Base(files[j])
		}
		return path.Base(files[i]) > path.Base(files[j])
	})
	mList := make(migrations, 0, len(files))
	for _, fileUp := range files {
		if strings.HasSuffix(fileUp, nameUp) {
			m := migration{
				fs:     fs,
				name:   strings.ReplaceAll(path.Base(fileUp), nameUp, ""),
				upPath: fileUp,
			}
			for _, fileDown := range files {
				if strings.HasSuffix(fileDown, m.name+nameDown) {
					m.downPath = fileDown
					break
				}
			}
			mList = append(mList, m)
		}
	}
	return mList
}

func (a migrations) onlyDown() migrations {
	result := make(migrations, 0, len(a))
	for _, m := range a {
		if len(m.downPath) > 0 {
			result = append(result, m)
		}
	}
	return result
}

func (a migrations) names() []string {
	result := make([]string, 0, len(a))
	for _, m := range a {
		result = append(result, m.name)
	}
	return result
}

func (a migration) Up(
	c context.Context,
) ([]string, error) {
	return a.cmd(c, a.upPath)
}

func (a migration) Down(
	c context.Context,
) ([]string, error) {
	if len(a.downPath) == 0 {
		return nil, errNoDown
	}
	return a.cmd(c, a.downPath)
}

func (a migration) cmd(
	c context.Context,
	path string,
) ([]string, error) {
	dataFile, err := a.fs.ReadFile(path)
	if err != nil {
		return nil, err
	}
	indexesCmd := regexCmd.FindAllSubmatchIndex(dataFile, -1)
	cmdResult := make([]string, 0, len(indexesCmd))
	for i := 0; i < len(indexesCmd); i++ {
		index := indexesCmd[i]
		cmd := exec.CommandContext(c, "sh", "-c", string(dataFile[index[2]:index[3]]))
		cmd.Stderr = os.Stderr
		pipe, err := cmd.StdoutPipe()
		if err != nil {
			return nil, err
		}
		if err = cmd.Start(); err != nil {
			return nil, err
		}
		cmdSql, err := io.ReadAll(pipe)
		if err != nil {
			return nil, err
		}
		if err = cmd.Wait(); err != nil {
			return nil, err
		}
		_ = pipe.Close()
		cmdResult = append(cmdResult, string(cmdSql))
	}
	for i := len(indexesCmd) - 1; i >= 0; i-- {
		index := indexesCmd[i]
		cmdSql := cmdResult[i]
		dataNew := make([]byte, 0, len(dataFile)+len(cmdSql))
		dataNew = append(dataNew, dataFile[0:index[0]]...)
		dataNew = append(dataNew, cmdSql...)
		dataNew = append(dataNew, dataFile[index[1]:]...)
		dataFile = dataNew
	}
	indexesDelimiter := regexDelimiter.FindAllIndex(dataFile, -1)
	dataList := make([][]byte, 0, len(indexesDelimiter)+1)
	dataList = append(dataList, dataFile)
	from := 0
	for _, index := range indexesDelimiter {
		dataList = append(dataList[0:len(dataList)-1], dataFile[from:index[0]], dataFile[index[0]:])
		from = index[0]
	}

	delimiter := []byte(";")
	result := make([]string, 0, len(dataList))
	for _, data := range dataList {
		if part := regexDelimiter.FindSubmatch(data); len(part) > 0 {
			delimiter = part[1]
		}
		queries := bytes.Split(
			regexCommentCleanup.ReplaceAll(data, []byte{}),
			delimiter,
		)
		for _, query := range queries {
			result = append(result, string(query))
		}
	}
	return result, nil
}
