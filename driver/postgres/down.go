package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/rathil/migrate/driver/internal"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
)

func (a *driver) DownFS(
	c context.Context,
	uri string,
	registryName string,
	fs filesystem.FS,
	count int,
	options ...internal.IOption,
) error {
	if count < 1 {
		return fmt.Errorf("not selected migration up to which you want to cancel")
	}
	files, err := helper.Find(fs, ".")
	if err != nil {
		return err
	}

	mList := createMigration(fs, files, false).onlyDown()
	if len(mList) == 0 {
		return nil
	}

	opt := mergeOptions(Option{
		LockId:    1,
		LockLevel: LockLevelTransaction,
	}, options)

	return a.inTxAndLock(c, uri, registryName, func(tx *sql.Tx) error {
		registryFindStmt, err := tx.PrepareContext(c, "select 1 from "+pq.QuoteIdentifier(registryName)+" where id = $1")
		if err != nil {
			return err
		}
		defer func() { _ = registryFindStmt.Close() }()

		mListNew := make(migrations, 0, len(mList))
		for _, m := range mList {
			exists := 0
			if errors.Is(registryFindStmt.QueryRowContext(c, m.name).Scan(&exists), sql.ErrNoRows) {
				continue
			}
			mListNew = append(mListNew, m)
			count--
			if count < 1 {
				break
			}
		}
		mList = mListNew

		return a.down(c, tx, registryName, mList)
	}, opt)
}

func (a *driver) Down(
	c context.Context,
	uri string,
	registryName string,
	path string,
	count int,
) error {
	return a.DownFS(c, uri, registryName, filesystem.FSPath(path), count)
}

func (a *driver) down(
	c context.Context,
	tx *sql.Tx,
	registryName string,
	mList migrations,
) error {
	if len(mList) == 0 {
		return nil
	}

	registryDeleteStmt, err := tx.PrepareContext(c, "delete from "+pq.QuoteIdentifier(registryName)+" where id = $1")
	if err != nil {
		return err
	}
	defer func() { _ = registryDeleteStmt.Close() }()

	for _, m := range mList {
		var queries []string
		queries, err = m.Down(c)
		if err != nil {
			return err
		}
		for _, query := range queries {
			_, err = tx.ExecContext(c, query)
			if err != nil {
				return fmt.Errorf("%w, in query: %s", err, query)
			}
		}
		if _, err = registryDeleteStmt.ExecContext(c, m.name); err != nil {
			return err
		}
	}
	return nil
}
