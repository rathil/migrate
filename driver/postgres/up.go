package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/lib/pq"
	"gitlab.com/rathil/migrate/driver/internal"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
)

func (a *driver) UpFS(
	c context.Context,
	uri string,
	registryName string,
	fs filesystem.FS,
	options ...internal.IOption,
) error {
	files, err := helper.Find(fs, ".")
	if err != nil {
		return err
	}
	mList := createMigration(fs, files, true)
	if len(mList) == 0 {
		return nil
	}

	opt := mergeOptions(Option{
		LockId:    1,
		LockLevel: LockLevelTransaction,
	}, options)

	return a.inTxAndLock(c, uri, registryName, func(tx *sql.Tx) error {
		registryFindStmt, err := tx.PrepareContext(c, "select 1 from "+pq.QuoteIdentifier(registryName)+" where id = $1")
		if err != nil {
			return err
		}
		defer func() { _ = registryFindStmt.Close() }()

		registryInsertStmt, err := tx.PrepareContext(c, "insert into "+pq.QuoteIdentifier(registryName)+" values($1, $2)")
		if err != nil {
			return err
		}
		defer func() { _ = registryInsertStmt.Close() }()

		for _, m := range mList {
			exists := 0
			if !errors.Is(registryFindStmt.QueryRowContext(c, m.name).Scan(&exists), sql.ErrNoRows) {
				continue
			}
			var queries []string
			queries, err = m.Up(c)
			if err != nil {
				return err
			}
			for _, query := range queries {
				if _, err = tx.ExecContext(c, query); err != nil {
					return fmt.Errorf("%w, in query: %s", err, query)
				}
			}
			if _, err = registryInsertStmt.ExecContext(c, m.name, time.Now().UTC()); err != nil {
				return err
			}
		}
		return nil
	}, opt)
}

func (a *driver) Up(
	c context.Context,
	uri string,
	registryName string,
	path string,
) (err_ error) {
	return a.UpFS(c, uri, registryName, filesystem.FSPath(path))
}
