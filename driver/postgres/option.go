package postgres

import (
	"database/sql"

	"gitlab.com/rathil/migrate/driver/internal"
)

type (
	Option struct {
		internal.Option
		// Id for pg_advisory_lock or pg_advisory_xact_lock
		LockId int64
		// Lock level
		LockLevel LockLevel
		// Self DB
		DB *sql.DB
		// Self transaction
		Tx *sql.Tx
	}
	LockLevel int
)

const (
	LockLevelSession     LockLevel = 1
	LockLevelTransaction LockLevel = 2
)

func MakeOption() Option {
	return Option{}
}

func (a Option) WithLockId(v int64) Option {
	a.LockId = v
	return a
}

func (a Option) WithLockLevelSession() Option {
	a.LockLevel = LockLevelSession
	return a
}

func (a Option) WithLockLevelTransaction() Option {
	a.LockLevel = LockLevelTransaction
	return a
}

func (a Option) WithDatabase(db *sql.DB) Option {
	a.DB = db
	return a
}

func (a Option) WithTransaction(tx *sql.Tx) Option {
	a.Tx = tx
	return a
}

// mergeOptions - combines the given options instances into a single option
func mergeOptions(defaultOption Option, options []internal.IOption) Option {
	for _, opt := range options {
		optPg, ok := opt.(Option)
		if !ok {
			continue
		}
		if optPg.LockId != 0 {
			defaultOption.LockId = optPg.LockId
		}
		if optPg.LockLevel != 0 {
			defaultOption.LockLevel = optPg.LockLevel
		}
		if optPg.DB != nil {
			defaultOption.DB = optPg.DB
		}
		if optPg.Tx != nil {
			defaultOption.Tx = optPg.Tx
		}
	}
	return defaultOption
}
