package postgres

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq"
)

func (a *driver) connect(
	c context.Context,
	uri string,
) (*sql.DB, error) {
	db, err := sql.Open("postgres", uri)
	if err != nil {
		return nil, err
	}
	if err = db.PingContext(c); err != nil {
		return nil, err
	}
	return db, nil
}
