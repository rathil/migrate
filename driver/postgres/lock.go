package postgres

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
)

func (a *driver) inTxAndLock(
	c context.Context,
	uri string,
	registryName string,
	cb func(tx *sql.Tx) error,
	opt Option,
) (err_ error) {
	tx := opt.Tx
	if tx == nil {
		if opt.DB == nil {
			opt.DB, err_ = a.connect(c, uri)
			if err_ != nil {
				return err_
			}
			defer func() { _ = opt.DB.Close() }()
		}

		tx, err_ = opt.DB.BeginTx(c, nil)
		if err_ != nil {
			return err_
		}
		defer func() {
			if err_ == nil {
				err_ = tx.Commit()
				return
			}
			_ = tx.Rollback()
		}()
	}

	if _, err := tx.ExecContext(c, `create table if not exists `+pq.QuoteIdentifier(registryName)+`
(
  id varchar not null primary key,
  migrated_at timestamp not null
)`,
	); err != nil {
		return err
	}

	switch opt.LockLevel {
	case LockLevelTransaction:
		if _, err := tx.ExecContext(c, "SELECT pg_advisory_xact_lock($1)", opt.LockId); err != nil {
			return err
		}
	case LockLevelSession:
		if _, err := tx.ExecContext(c, "SELECT pg_advisory_lock($1)", opt.LockId); err != nil {
			return err
		}
		defer func() {
			_, _ = tx.ExecContext(context.WithoutCancel(c), "SELECT pg_advisory_unlock($1)", opt.LockId)
		}()
	}

	return cb(tx)
}
