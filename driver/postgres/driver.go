package postgres

import (
	driverNs "gitlab.com/rathil/migrate/driver"
)

func Get() driverNs.IDriver {
	return &driver{}
}

type driver struct{}

func (a *driver) Name() string {
	return "PostgreSQL"
}

const (
	nameUp   = ".up.pg.sql"
	nameDown = ".down.pg.sql"
)
