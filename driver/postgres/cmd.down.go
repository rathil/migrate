package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/AlecAivazis/survey/v2"
	"github.com/lib/pq"
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
)

func (a *driver) CmdDown() *cli.Command {
	return &cli.Command{
		Name:    "postgres",
		Aliases: []string{"postgre", "postgresql", "pg"},
		Usage:   "Cancel postgres migration",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "uri",
				Usage:    "Database connection string",
				Required: true,
			},
			&cli.UintFlag{
				Name:  "count",
				Usage: "Number of migrations to cancel",
			},
			&cli.StringFlag{
				Name:  "name",
				Usage: "The name of the migration up to which you want to cancel",
			},
		},
		Action: func(ctx *cli.Context) error {
			fs := filesystem.FSPath(ctx.String("path"))
			files, err := helper.Find(fs, ".")
			if err != nil {
				return err
			}
			mList := createMigration(fs, files, false).onlyDown()
			if len(mList) == 0 {
				return nil
			}

			c := ctx.Context
			registryName := ctx.String("registry-name")

			opt := Option{
				LockId:    1,
				LockLevel: LockLevelTransaction,
			}

			return a.inTxAndLock(c, ctx.String("uri"), registryName, func(tx *sql.Tx) error {
				registryFindStmt, err := tx.PrepareContext(c, "select 1 from "+pq.QuoteIdentifier(registryName)+" where id = $1")
				if err != nil {
					return err
				}
				defer func() { _ = registryFindStmt.Close() }()

				mListNew := make(migrations, 0, len(mList))
				for _, m := range mList {
					exists := 0
					if errors.Is(registryFindStmt.QueryRowContext(c, m.name).Scan(&exists), sql.ErrNoRows) {
						continue
					}
					mListNew = append(mListNew, m)
				}
				mList = mListNew

				count := ctx.Int("count")
				if count < 1 {
					name := ctx.String("name")
					count = a.countDownByName(mList, name)
					if count < 1 {
						if err := survey.AskOne(&survey.Select{
							Message: "Select the migration up to which you want to cancel:",
							Options: mList.names(),
						}, &name); err != nil {
							return err
						}
						count = a.countDownByName(mList, name)
						if count < 1 {
							return fmt.Errorf("not selected migration up to which you want to cancel")
						}
					}
				}
				return a.down(c, tx, registryName, mList[:count])
			}, opt)
		},
	}
}

func (a *driver) countDownByName(
	mList migrations,
	cancelToName string,
) (count_ int) {
	for _, m := range mList {
		count_++
		if m.name == cancelToName {
			return
		}
	}
	return 0
}
