package postgres

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"os"
	"path"
	"time"
)

var (
	sqlUp = []byte(`-- if you want to change the query delimiter, leave this comment and change the delimiter to the needed one
-- +delimiter _
-- +cmd echo "select 1_ select 2 _"
-- 
-- +delimiter ;
-- +cmd echo "select 1; select 2;"
-- 
-- Example:
-- create table device_backup
-- (
-- 	id bigint not null constraint device_backup_pk primary key,
-- 	name varchar
-- );
-- comment on table device_backup is 'Backup for table "device" before delete field "name"';
-- comment on column device_backup.id is 'Original device id';
-- comment on column device_backup.name is 'The name of the device to be saved until the next release';
-- 
-- insert into device_backup select id, name from device;
-- alter table device drop column name;
`)
	sqlDown = []byte(`-- if you want to change the query delimiter, leave this comment and change the delimiter to the needed one
-- +delimiter ;
-- 
-- Example:
-- alter table device add name varchar;
-- update device d set name = db.name from device_backup db where d.id = db.id;
-- drop table device_backup;
`)
)

func (a *driver) CmdCreate() *cli.Command {
	return &cli.Command{
		Name:    "postgres",
		Aliases: []string{"postgre", "postgresql", "pg"},
		Usage:   "Create postgres migration",
		Action: func(ctx *cli.Context) error {
			t := time.Now().UTC().Format("20060102150405")
			name := ctx.String("name")
			{
				filePath := path.Join(ctx.String("path"), fmt.Sprintf(
					"%s_%s%s", t, name, nameUp,
				))
				if err := os.WriteFile(filePath, sqlUp, 0600); err != nil {
					return err
				}
				fmt.Println("Created:", filePath)
			}
			{
				filePath := path.Join(ctx.String("path"), fmt.Sprintf(
					"%s_%s%s", t, name, nameDown,
				))
				if err := os.WriteFile(filePath, sqlDown, 0600); err != nil {
					return err
				}
				fmt.Println("Created:", filePath)
			}
			return nil
		},
	}
}
