package mongodb

import (
	"time"
)

type registry struct {
	Id        string    `bson:"_id"`
	MigrateAt time.Time `bson:"migrate_at"`
}
