package mongodb

import "go.mongodb.org/mongo-driver/bson"

type command struct {
	Transaction bool     `json:"transaction"`
	Commands    []bson.D `json:"commands"`
	Cmd         []string `json:"cmd"`
	Comments    string   `json:"comments"`
}
