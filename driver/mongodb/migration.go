package mongodb

import (
	"context"
	"errors"
	"io"
	"os"
	"os/exec"
	"path"
	"sort"
	"strings"

	"gitlab.com/rathil/migrate/filesystem"
	"go.mongodb.org/mongo-driver/bson"
)

type migrations []migration
type migration struct {
	fs       filesystem.FS
	name     string
	upPath   string
	downPath string
}

var errNoDown = errors.New("no migrations to cancel")

func createMigration(
	fs filesystem.FS,
	files []string,
	orderAsc bool,
) migrations {
	sort.SliceStable(files, func(i, j int) bool {
		if orderAsc {
			return path.Base(files[i]) < path.Base(files[j])
		}
		return path.Base(files[i]) > path.Base(files[j])
	})
	mList := make(migrations, 0, len(files))
	for _, fileUp := range files {
		if strings.HasSuffix(fileUp, nameUp) {
			m := migration{
				fs:     fs,
				name:   strings.ReplaceAll(path.Base(fileUp), nameUp, ""),
				upPath: fileUp,
			}
			for _, fileDown := range files {
				if strings.HasSuffix(fileDown, m.name+nameDown) {
					m.downPath = fileDown
					break
				}
			}
			mList = append(mList, m)
		}
	}
	return mList
}

func (a migrations) onlyDown() migrations {
	result := make(migrations, 0, len(a))
	for _, m := range a {
		if len(m.downPath) > 0 {
			result = append(result, m)
		}
	}
	return result
}

func (a migrations) names() []string {
	result := make([]string, 0, len(a))
	for _, m := range a {
		result = append(result, m.name)
	}
	return result
}

func (a migration) Up(
	c context.Context,
) (command, error) {
	return a.cmd(c, a.upPath)
}

func (a migration) Down(
	c context.Context,
) (command, error) {
	if len(a.downPath) == 0 {
		return command{}, errNoDown
	}
	return a.cmd(c, a.downPath)
}

func (a migration) cmd(
	c context.Context,
	path string,
) (command, error) {
	data, err := a.fs.ReadFile(path)
	if err != nil {
		return command{}, err
	}
	cmdObj := command{}
	if err := bson.UnmarshalExtJSON(data, true, &cmdObj); err != nil {
		return command{}, err
	}
	for _, cmdLine := range cmdObj.Cmd {
		cmd := exec.CommandContext(c, "sh", "-c", cmdLine)
		cmd.Stderr = os.Stderr
		pipe, err := cmd.StdoutPipe()
		if err != nil {
			return command{}, err
		}
		if err = cmd.Start(); err != nil {
			return command{}, err
		}
		cmdCommand, err := io.ReadAll(pipe)
		if err != nil {
			return command{}, err
		}
		if err = cmd.Wait(); err != nil {
			return command{}, err
		}
		_ = pipe.Close()
		var cmdCommands []bson.D
		if err = bson.UnmarshalExtJSON(cmdCommand, true, &cmdCommands); err != nil {
			return command{}, err
		}
		cmdObj.Commands = append(cmdObj.Commands, cmdCommands...)
	}
	return cmdObj, nil
}
