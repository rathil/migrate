package mongodb

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
)

func (a *driver) connect(
	c context.Context,
	uri string,
	registryName string,
) (*mongo.Database, *mongo.Collection, error) {
	cfg, err := connstring.ParseAndValidate(uri)
	if err != nil {
		return nil, nil, err
	}
	client, err := mongo.Connect(c, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, err
	}
	if err = client.Ping(c, nil); err != nil {
		return nil, nil, err
	}
	db := client.Database(cfg.Database)
	registryCollection := db.Collection(registryName)
	var registryItem registry
	if err = registryCollection.FindOne(c, bson.M{}).Decode(&registryItem); err != nil && !errors.Is(err, mongo.ErrNoDocuments) {
		return nil, nil, err
	}
	return db, registryCollection, nil
}
