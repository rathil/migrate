package mongodb

import (
	"context"
	"errors"
	"log"
	"time"

	"gitlab.com/rathil/migrate/driver/internal"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (a *driver) UpFS(
	c context.Context,
	uri string,
	registryName string,
	fs filesystem.FS,
	_ ...internal.IOption,
) (err_ error) {
	files, err := helper.Find(fs, ".")
	if err != nil {
		return err
	}
	mList := createMigration(fs, files, true)
	if len(mList) == 0 {
		return nil
	}
	db, registryCollection, err := a.connect(c, uri, registryName)
	if err != nil {
		return err
	}
	migrationDown := make(migrations, 0, len(mList))
	defer func() {
		for i := len(migrationDown) - 1; i >= 0; i-- {
			if err_ != nil {
				if err = a.downOne(c, db, registryCollection, migrationDown[i]); err != nil {
					log.Printf("down migration %s, err: %s\n", migrationDown[i].name, err)
				}
			}
		}
		_ = db.Client().Disconnect(context.WithoutCancel(c))
	}()
	for _, m := range mList {
		if !errors.Is(registryCollection.FindOne(c, bson.M{"_id": m.name}).Err(), mongo.ErrNoDocuments) {
			continue
		}
		cmdObj, err := m.Up(c)
		if err != nil {
			return err
		}
		if cmdObj.Transaction {
			if err = db.Client().UseSession(c, func(sessionContext mongo.SessionContext) error {
				if err = sessionContext.StartTransaction(); err != nil {
					return err
				}
				for _, command := range cmdObj.Commands {
					if err := db.RunCommand(sessionContext, command).Err(); err != nil {
						return err
					}
				}
				return sessionContext.CommitTransaction(sessionContext)
			}); err != nil {
				return err
			}
		} else {
			for _, command := range cmdObj.Commands {
				if err := db.RunCommand(c, command).Err(); err != nil {
					return err
				}
			}
		}
		if _, err = registryCollection.InsertOne(c, registry{
			Id:        m.name,
			MigrateAt: time.Now().UTC(),
		}); err != nil {
			return err
		}
		migrationDown = append(migrationDown, m)
	}
	return nil
}

func (a *driver) Up(
	c context.Context,
	uri string,
	registryName string,
	path string,
) (err_ error) {
	return a.UpFS(c, uri, registryName, filesystem.FSPath(path))
}
