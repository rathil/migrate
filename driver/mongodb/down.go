package mongodb

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/rathil/migrate/driver/internal"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (a *driver) DownFS(
	c context.Context,
	uri string,
	registryName string,
	fs filesystem.FS,
	count int,
	_ ...internal.IOption,
) error {
	if count < 1 {
		return fmt.Errorf("not selected migration up to which you want to cancel")
	}
	files, err := helper.Find(fs, ".")
	if err != nil {
		return err
	}

	db, registryCollection, err := a.connect(c, uri, registryName)
	if err != nil {
		return err
	}
	defer func() { _ = db.Client().Disconnect(context.WithoutCancel(c)) }()

	mList := createMigration(fs, files, false).onlyDown()
	mListNew := make(migrations, 0, len(mList))
	for _, m := range mList {
		if errors.Is(registryCollection.FindOne(c, bson.M{"_id": m.name}).Err(), mongo.ErrNoDocuments) {
			continue
		}
		mListNew = append(mListNew, m)
		count--
		if count < 1 {
			break
		}
	}
	mList = mListNew

	return a.down(
		c,
		db,
		registryCollection,
		mList,
	)
}

func (a *driver) Down(
	c context.Context,
	uri string,
	registryName string,
	path string,
	count int,
) error {
	return a.DownFS(c, uri, registryName, filesystem.FSPath(path), count)
}

func (a *driver) down(
	c context.Context,
	db *mongo.Database,
	registryCollection *mongo.Collection,
	mList migrations,
) error {
	for _, m := range mList {
		if err := a.downOne(c, db, registryCollection, m); err != nil {
			return err
		}
	}
	return nil
}

func (a *driver) downOne(
	c context.Context,
	db *mongo.Database,
	registryCollection *mongo.Collection,
	m migration,
) error {
	cmdObj, err := m.Down(c)
	if err != nil {
		return err
	}
	if cmdObj.Transaction {
		if err = db.Client().UseSession(c, func(sessionContext mongo.SessionContext) error {
			if err := sessionContext.StartTransaction(); err != nil {
				return err
			}
			for _, command := range cmdObj.Commands {
				if err := db.RunCommand(sessionContext, command).Err(); err != nil {
					return err
				}
			}
			return sessionContext.CommitTransaction(sessionContext)
		}); err != nil {
			return err
		}
	} else {
		for _, command := range cmdObj.Commands {
			if err = db.RunCommand(c, command).Err(); err != nil {
				return err
			}
		}
	}
	_, err = registryCollection.DeleteOne(c, bson.M{"_id": m.name})
	return err
}
