package mongodb

import (
	"context"
	"errors"
	"fmt"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/migrate/filesystem"
	"gitlab.com/rathil/migrate/helper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (a *driver) CmdDown() *cli.Command {
	return &cli.Command{
		Name:  "mongodb",
		Usage: "Cancel mongodb migration",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "uri",
				Usage:    "Database connection string",
				Required: true,
			},
			&cli.UintFlag{
				Name:  "count",
				Usage: "Number of migrations to cancel",
			},
			&cli.StringFlag{
				Name:  "name",
				Usage: "The name of the migration up to which you want to cancel",
			},
		},
		Action: func(ctx *cli.Context) error {
			fs := filesystem.FSPath(ctx.String("path"))
			files, err := helper.Find(fs, ".")
			if err != nil {
				return err
			}
			mList := createMigration(fs, files, false).onlyDown()
			if len(mList) == 0 {
				return nil
			}

			c := ctx.Context

			db, registryCollection, err := a.connect(
				c,
				ctx.String("uri"),
				ctx.String("registry-name"),
			)
			if err != nil {
				return err
			}
			defer func() { _ = db.Client().Disconnect(context.WithoutCancel(c)) }()

			mListNew := make(migrations, 0, len(mList))
			for _, m := range mList {
				if errors.Is(registryCollection.FindOne(c, bson.M{"_id": m.name}).Err(), mongo.ErrNoDocuments) {
					continue
				}
				mListNew = append(mListNew, m)
			}
			mList = mListNew

			count := ctx.Int("count")
			if count < 1 {
				name := ctx.String("name")
				count = a.countDownByName(mList, name)
				if count < 1 {
					if err := survey.AskOne(&survey.Select{
						Message: "Select the migration up to which you want to cancel:",
						Options: mList.names(),
					}, &name); err != nil {
						return err
					}
					count = a.countDownByName(mList, name)
					if count < 1 {
						return fmt.Errorf("not selected migration up to which you want to cancel")
					}
				}
			}
			return a.down(c, db, registryCollection, mList[:count])
		},
	}
}

func (a *driver) countDownByName(
	mList migrations,
	cancelToName string,
) (count_ int) {
	for _, m := range mList {
		count_++
		if m.name == cancelToName {
			return
		}
	}
	return 0
}
