package mongodb

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
	"time"

	"github.com/urfave/cli/v2"
	"go.mongodb.org/mongo-driver/bson"
)

func (a *driver) CmdCreate() *cli.Command {
	return &cli.Command{
		Name:  "mongodb",
		Usage: "Create mongodb migration",
		Action: func(ctx *cli.Context) error {
			t := time.Now().UTC().Format("20060102150405")
			name := ctx.String("name")
			{
				filePath := path.Join(ctx.String("path"), fmt.Sprintf(
					"%s_%s%s", t, name, nameUp,
				))
				data, err := json.MarshalIndent(command{
					Transaction: true,
					Commands:    []bson.D{},
					Cmd:         []string{},
					Comments:    "https://docs.mongodb.com/manual/reference/command/",
				}, "", "  ")
				if err != nil {
					return err
				}
				if err = os.WriteFile(filePath, data, 0600); err != nil {
					return err
				}
				fmt.Println("Created:", filePath)
			}
			{
				filePath := path.Join(ctx.String("path"), fmt.Sprintf(
					"%s_%s%s", t, name, nameDown,
				))
				data, err := json.MarshalIndent(command{
					Transaction: true,
					Commands:    []bson.D{},
					Cmd:         []string{},
					Comments:    "https://docs.mongodb.com/manual/reference/command/",
				}, "", "  ")
				if err != nil {
					return err
				}
				if err = os.WriteFile(filePath, data, 0600); err != nil {
					return err
				}
				fmt.Println("Created:", filePath)
			}
			fmt.Println("Available commands: https://docs.mongodb.com/manual/reference/command/")
			return nil
		},
	}
}
