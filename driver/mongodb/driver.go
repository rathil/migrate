package mongodb

import (
	driverNs "gitlab.com/rathil/migrate/driver"
)

func Get() driverNs.IDriver {
	return &driver{}
}

type driver struct{}

func (a *driver) Name() string {
	return "MongoDB"
}

const (
	nameUp   = ".up.mongodb.json"
	nameDown = ".down.mongodb.json"
)
