package mongodb

import (
	"github.com/urfave/cli/v2"
)

func (a *driver) CmdUp() *cli.Command {
	return &cli.Command{
		Name:  "mongodb",
		Usage: "Applying mongodb migration",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "uri",
				Usage:    "Database connection string",
				Required: true,
			},
		},
		Action: func(ctx *cli.Context) error {
			return a.Up(
				ctx.Context,
				ctx.String("uri"),
				ctx.String("registry-name"),
				ctx.String("path"),
			)
		},
	}
}
