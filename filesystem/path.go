package filesystem

import (
	"os"
)

func FSPath(path string) FS {
	return os.DirFS(path).(FS)
}
