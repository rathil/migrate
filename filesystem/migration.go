package filesystem

import (
	"io"
	"io/fs"
	"path"
	"time"
)

func NewMigration(
	name string,
	reader io.Reader,
) *Migration {
	return &Migration{
		name:   name,
		Reader: reader,
	}
}

type Migration struct {
	name string
	io.Reader
}

func (a *Migration) Name() string               { return path.Base(a.name) }
func (a *Migration) IsDir() bool                { return false }
func (a *Migration) Type() fs.FileMode          { return a.Mode().Type() }
func (a *Migration) Info() (fs.FileInfo, error) { return a, nil }
func (a *Migration) Mode() fs.FileMode          { return 0444 }
func (a *Migration) Size() int64                { return 0 }
func (a *Migration) ModTime() time.Time         { return time.Time{} }
func (a *Migration) Sys() any                   { return nil }
func (a *Migration) Stat() (fs.FileInfo, error) { return a, nil }
func (a *Migration) Close() error               { return nil }
