package filesystem

import (
	"io"
	"io/fs"
	"strings"
)

func FSCode(items ...*Migration) *Code {
	return &Code{
		items: items,
	}
}

type Code struct {
	items []*Migration
}

func (a *Code) Open(name string) (file_ fs.File, err_ error) {
	for _, item := range a.items {
		if item.name == name {
			return item, nil
		}
	}
	return nil, fs.ErrNotExist
}

func (a *Code) ReadDir(name string) ([]fs.DirEntry, error) {
	var result []fs.DirEntry
	for _, item := range a.items {
		if strings.HasPrefix(item.name, name) || name == "." {
			result = append(result, item)
		}
	}
	return result, nil
}

func (a *Code) ReadFile(name string) (data_ []byte, err_ error) {
	for _, item := range a.items {
		if item.name == name {
			return io.ReadAll(item)
		}
	}
	return nil, fs.ErrNotExist
}
