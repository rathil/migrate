package filesystem

import (
	"errors"
	"io/fs"
)

func FSJoin(items ...FS) *Join {
	return &Join{
		items: items,
	}
}

type Join struct {
	items []FS
}

func (a *Join) Add(fs FS) *Join {
	a.items = append(a.items, fs)
	return a
}

func (a *Join) Open(name string) (file_ fs.File, err_ error) {
	for _, item := range a.items {
		if file_, err_ = item.Open(name); err_ == nil {
			return
		}
	}
	return
}

func (a *Join) ReadDir(name string) ([]fs.DirEntry, error) {
	var result []fs.DirEntry
	for _, item := range a.items {
		items, err := item.ReadDir(name)
		if err != nil {
			if errors.Is(err, fs.ErrNotExist) {
				continue
			}
			return nil, err
		}
		result = append(result, items...)
	}
	return result, nil
}

func (a *Join) ReadFile(name string) (data_ []byte, err_ error) {
	for _, item := range a.items {
		if data_, err_ = item.ReadFile(name); err_ == nil {
			return
		}
	}
	return
}
